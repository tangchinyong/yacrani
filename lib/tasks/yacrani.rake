require 'net/http'
require 'dcf'
require 'zlib' 
require 'json'
require 'date'


CRAN_ROOT_DOMAIN = 'https://cran.r-project.org'
CRAN_BASE_PATH = '/src/contrib/'
PACKAGE_ZIP_FILENAME = 'PACKAGES.gz'
PACKAGE_FILENAME = 'PACKAGES'

PROCESSING_LIMIT = 50


desc "Yet Another CRAN Indexer"

task :yacrani => :environment do

  LOCAL_SAVE_ROOT_PATH = Rails.root.join('tmp')
  LOCAL_TAR_PATH = LOCAL_SAVE_ROOT_PATH + 'tar/'
  LOCAL_EXTRACT_PATH = LOCAL_SAVE_ROOT_PATH + 'extract/'

  def download_and_save_uri(url, save_path)
    puts "Downloading #{url} and saving to #{save_path}"
    if File.file? save_path
      return
    end
    uri = URI(url)
    res = res = Net::HTTP.get_response(uri)

    f = File.new(save_path, "wb")
    f.write(res.body)
  end

  # Unzip the main Package file
  def unzip(path_zip, path_unzip)
    puts "Unzipping #{path_zip} to #{path_unzip}"
    if File.file? path_unzip
      return
    end
    gz = Zlib::GzipReader.new(File.open(path_zip))
    result = gz.read
    File.write(path_unzip, result).close
  end

  # implict function name, rename if there is a  elegant name
  def commandline_tar_unzip(path_zip, extract_path, path_unzip) 
    if File.directory? path_unzip
      puts "Unzip file exists => #{path_unzip}"
      return
    end

    puts "unzipping: tar -xzvf #{path_zip} -C #{extract_path}"
    `tar -xzvf #{path_zip} -C #{extract_path}`
  end

  # implict function name, rename if there is a elegant name
  def read_dcf(filename, limit = nil)
    segment = ""
    count = 0
    
    File.foreach(filename) do |l| 
      segment = segment + l

      if l.strip.empty?    # keep a count to limit the number of items to process, for development
        count = count + 1
      end

      if !limit.nil? && count >= limit
        break
      end
    end
    
    Dcf.parse(segment)
  end

  #Check for tar and extract folders under tmp
  Dir.mkdir LOCAL_TAR_PATH if !File.directory? LOCAL_TAR_PATH
  Dir.mkdir LOCAL_EXTRACT_PATH if !File.directory? LOCAL_EXTRACT_PATH


  #Download package and unzip
  local_package_zip_path =  LOCAL_SAVE_ROOT_PATH + PACKAGE_ZIP_FILENAME
  local_package_path = LOCAL_EXTRACT_PATH + PACKAGE_FILENAME

  uri = URI(CRAN_ROOT_DOMAIN + CRAN_BASE_PATH + PACKAGE_ZIP_FILENAME)

  download_and_save_uri(uri, local_package_zip_path)
  unzip(local_package_zip_path, local_package_path)

  dcf_list = read_dcf(local_package_path, PROCESSING_LIMIT)

  DISPLAY_KEYS = ['Title', 'Version', 'Description', 'Author', 'Maintainer', "Date/Publication"]

  dcf_list.each do | obj | 

    package_name = obj['Package']
    version = obj['Version']

    puts "Indexing #{package_name}"
    uri_zip = CRAN_ROOT_DOMAIN + CRAN_BASE_PATH + package_name + '_' + version + '.tar.gz'
    path_zip = "#{LOCAL_TAR_PATH}#{package_name}_#{version}.tar.gz"
    path_unzip = "#{LOCAL_EXTRACT_PATH}#{package_name}"

    download_and_save_uri(uri_zip, path_zip)
    commandline_tar_unzip(path_zip, LOCAL_EXTRACT_PATH, path_unzip)

    description_file = path_unzip + '/DESCRIPTION'

    package_details = read_dcf(description_file, 1)

    package = {name: package_name, version: version}
    DISPLAY_KEYS.each do |k|
      package[k.downcase.to_sym] = package_details.first[k] #treetop_dcf output is an array of hashes
    end
    package[:date_publication] = DateTime.parse(package.delete("date/publication".to_sym)).iso8601

    maintainers = Person.parse_maintainers_raw_input package.delete(:maintainer)
    authors = Person.parse_authors_raw_input package.delete(:author)

    package_object = Package.where({name: package[:name], version: package[:version]}).first_or_create(package)

    maintainers.each do |o|
      person_object = Person.where(o).first_or_create

      m = Maintainer.where({package: package_object.id, person: person_object.id}).first_or_create
    end

    authors.each do |o|
      person_object = Person.where({name: o[:name]}).first_or_create

      m = Author.where({package: package_object.id, person: person_object.id}).first_or_create
    end
  end
end

