desc "Clear Extract Folder"

task :ClearExtract => :environment do

  LOCAL_SAVE_ROOT_PATH = Rails.root.join('tmp')
  LOCAL_EXTRACT_PATH = LOCAL_SAVE_ROOT_PATH + 'extract/'	

	puts "Removing local extract folder: rm -r #{LOCAL_EXTRACT_PATH}"
  `rm -r #{LOCAL_EXTRACT_PATH}`
end

