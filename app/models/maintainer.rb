class Maintainer < ApplicationRecord
  belongs_to :package
  belongs_to :person
end
