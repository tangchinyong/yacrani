class Person < ApplicationRecord
  has_many :authors
  has_many :maintainers
  has_many :packages, through: :authors
  has_many :packages, through: :maintainers

  validates :name, presence: true
  validates :name, uniqueness: true 
  validates :email, uniqueness: true, allow_nil: true


  def self.parse_maintainers_raw_input(string_of_m) #expect a string of maintainers
  	#sampled dcf entries seems to indicate only one maintainer, however covering the multi maintainer scenario
  	string_of_m.gsub(' and ',',').split(",").map do |x|
  		x = x.strip
      reg = /(.*?)\s<(.*?)>/.match x
      {name: reg[1], email: reg[2]}
    end
  end

  def self.parse_authors_raw_input(string_of_a) #expect a straing of authors
  	# assumption: Remove all meta information
  	# replace 'and', '&' with ','
  	# strip [...text...] and (...text...) and <...text...>
  	string_of_a.gsub(/( and |&)/,',').gsub(/(\[.*?\]|\(.*?\)|\<.*?\>)/,"").split(",").map { |x| {:name => x.strip} }
  end
end
