class Package < ApplicationRecord
  has_many :authors
  has_many :maintainers
  has_many :people, through: :authors
  has_many :people, through: :maintainers

  validates :name, :version, :date_publication, :title, :description, presence: true

  #assume that name + version  is unique 
  validates :name, uniqueness: {scope: :version, message: "Package name & version should be unique"}

end
