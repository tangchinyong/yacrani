require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end


  test "parse_maintainers_raw_input" do
  	raw_input = "Fabio Mathias Correa <fmcorrea@uesc.br>"
  	expected_result = [{name: "Fabio Mathias Correa", email: "fmcorrea@uesc.br"}]
  	res = Person.parse_maintainers_raw_input raw_input
  	assert_equal res, expected_result
  end

  test "parse_maintainers_raw_input and handles for 'and'" do
  	raw_input = "Alexander Yermanos <ayermano@ethz.ch>"
  	expected_result = [{name: "Alexander Yermanos", email: "ayermano@ethz.ch"}]
  	res = Person.parse_maintainers_raw_input raw_input
  	assert_equal res, expected_result
  end

  test "parse_authors_raw_input and handles for '&'" do
  	raw_input = "Antony M. Overstall ,Maria Adamou & Damianos Michaelides"
  	expected_result = [{name: "Antony M. Overstall"}, {name: "Maria Adamou"}, {name: "Damianos Michaelides"}]
  	res = Person.parse_authors_raw_input raw_input
  	assert_equal res, expected_result
	end

  test "parse_authors_raw_input and handles for 'and'" do
  	raw_input = "Antony M. Overstall ,Maria Adamou and Damianos Michaelides"
  	expected_result = [{name: "Antony M. Overstall"}, {name: "Maria Adamou"}, {name: "Damianos Michaelides"}]
  	res = Person.parse_authors_raw_input raw_input
  	assert_equal res, expected_result
	end

  test "parse_authors_raw_input and strips away meta" do
  	raw_input = "Gilles Kratzer [aut, cre] (<https://orcid.org/0000-0002-5929-8935>), \
  	Fraser Ian Lewis [aut], Reinhard Furrer [ctb] (<https://orcid.org/0000-0002-6319-2332>), \ 
  	Marta Pittavino [ctb] (<https://orcid.org/0000-0002-1232-1034>)"
  	expected_result = [{name: "Gilles Kratzer"}, {name: "Fraser Ian Lewis"}, {name: "Reinhard Furrer"}, {name: "Marta Pittavino"}]
  	res = Person.parse_authors_raw_input raw_input
  	assert_equal res, expected_result
  end

  test "parse_authors_raw_input and strips away email" do
  	raw_input ="Jung Ae Lee <julee@uark.edu>"
  	expected_result = [{name: "Jung Ae Lee"}]
  	res = Person.parse_authors_raw_input raw_input
  	assert_equal res, expected_result
  end
end
