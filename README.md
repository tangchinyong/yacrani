# README


### SPECIAL NOTE:
dcf_treetop do no seems to work out of the box via bundle

To document the time sink:

After bundle and if dcf cannot be required, do the following:
update ```/#{USER_INSTALL_PATH}/.rvm/gems/ruby-2.6.5/gems/treetop-dcf-0.2.1/lib/dcf.rb```

Change line 3:

change
```require File.join(File.dirname(__FILE__) + "/dcf_grammar")```
to
```Treetop.load File.join(File.dirname(__FILE__) + "/dcf_grammar")```

Example of the changes can be review at https://bitbucket.org/tangchinyong/treetop-dcf/diff/lib/dcf.rb?at=master&diff2=01586ea44ec4d6740e29d4eb919d3befb36cb7d9



#### Check out the repo
```git clone git@bitbucket.org:tangchinyong/yacrani.git```

#### Assuming using rvm and ruby 2.6.5 with mysqlite locally
```bundle install```
```yarn install --check-files```

#### Setup the DB
```rails db:migrate```
#### Run the tests just for the sake of it
```rails test```

#### Run the rake task
```rake yacrani```

If the rake task fail while download or indexing a package, simply rerunning the rake task will alow the job to continue. If the downloaded file is partial and corrupted, delete the download package and the respecive extracted folder and rerun the rake task. CRAN network seems to be sensitive to unstable network.


#### To configure it as a cron job daily
Add the following output of whenever gem to the crontab
run ```bundle exec whenever``` and add the output to cron




### Whats next?
The main rake task can be optimised to NOT download and uncompressed another package entry by checking if the package entry (name + version) exists in database. This should reduce the verbosity of the stdout. Note that the most "costly" operation is downloading the zip files, and the script do not purge the zip files currently. 

We can also consider seperating the jobs to have one portion that does batch downloading of packages zips before uncompressing. This might greatly reduce the number of failures of the jobs due to the script not able to find the "description" file.

The task currently handles only the happy path, some error handling can be put in place. 2 areas, ensuring the package zips and downloaded, and ensuring the uncompress is successful.

The 2 methods ```parse_maintainers_raw_input``` and ```parse_authors_raw_input``` are placed under the Person model. That can be revisited as it is not really Model related. The methods are there just for pragmatic reasons.


