class CreatePackages < ActiveRecord::Migration[6.0]
  def change
    create_table :packages do |t|
      t.string :name
      t.string :version
      t.timestamp :date_publication
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
