class PackageChangeDescriptionTypeToText < ActiveRecord::Migration[6.0]
 def up
    change_column :packages, :description, :text
  end
  
  def down
    change_column :packages, :description, :string
  end
end
